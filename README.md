# Leetcode 解题报告

## 典型题目

### 链表 (Linked List)

- [2. Add Two Numbers](posts/2.md)
- [19. Remove Nth Node From End of List](posts/19.md)
- [23. Merge k Sorted Lists](posts/23.md)
- [24. Swap Nodes in Pairs](posts/24.md)
- [25. Reverse Nodes in k-Group](posts/25.md)
- [61. Rotate List](posts/61.md)
- [82. Remove Duplicates from Sorted List II](posts/82.md)
- [92. Reverse Linked List II](posts/92.md)
- [138. Copy List with Random Pointer](posts/138.md)
- [142. Linked List Cycle II](posts/142.md)
- [143. Reorder List](posts/143.md)
- [146. LRU Cache](posts/146.md)
- [147. Insertion Sort List](posts/147.md)
- [148. Sort List](posts/148.md)
- [430. Flatten a Multilevel Doubly Linked List](posts/430.md)

### 双指针 (Two Pointers)

#### 同向双指针

- [3. Longest Substring Without Repeating Characters](posts/3.md)
- [15. 3Sum](posts/15.md)
- [16. 3Sum Closest](posts/16.md)
- [18. 4Sum](posts/18.md)
- [76. Minimum Window Substring](posts/76.md)
- [159. Longest Substring with At Most Two Distinct Characters](posts/159.md)
- [209. Minimum Size Subarray Sum](posts/209.md)
- [340. Longest Substring with At Most K Distinct Characters](posts/340.md)
- [413. Arithmetic Slices](posts/413.md)
- [454. 4Sum II](posts/454.md)
- [481. Magical String](posts/481.md)

#### 反向双指针

- [11. Container With Most Water](posts/11.md)

#### 滑动窗 (Sliding Window)

- [30. Substring with Concatenation of All Words](posts/30.md)
- [1423. Maximum Points You Can Obtain from Cards](posts/1423.md)

### 二叉树 (Binary Tree)

- [94. Binary Tree Inorder Traversal](posts/94.md)
- [95. Unique Binary Search Trees II](posts/95.md)
- [98. Validate Binary Search Tree](posts/98.md)
- [99. Recover Binary Search Tree](posts/99.md)
- [102. Binary Tree Level Order Traversal](posts/102.md)
- [103. Binary Tree Zigzag Level Order Traversal](posts/103.md)
- [105. Construct Binary Tree from Preorder and Inorder Traversal](posts/105.md)
- [106. Construct Binary Tree from Inorder and Postorder Traversal](posts/106.md)
- [109. Convert Sorted List to Binary Search Tree](posts/109.md)
- [113. Path Sum II](posts/113.md)
- [114. Flatten Binary Tree to Linked List](posts/114.md)
- [116. Populating Next Right Pointers in Each Node](posts/116.md)
- [117. Populating Next Right Pointers in Each Node II](posts/117.md)
- [124. Binary Tree Maximum Path Sum](posts/124.md)
- [129. Sum Root to Leaf Numbers](posts/129.md)
- [144. Binary Tree Preorder Traversal](posts/144.md)
- [156. Binary Tree Upside Down](posts/156.md)
- [173. Binary Search Tree Iterator](posts/173.md)
- [199. Binary Tree Right Side View](posts/199.md)
- [222. Count Complete Tree Nodes](posts/222.md)
- [230. Kth Smallest Element in a BST](posts/230.md)
- [236. Lowest Common Ancestor of a Binary Tree](posts/236.md)
- [250. Count Univalue Subtrees](posts/250.md)
- [255. Verify Preorder Sequence in Binary Search Tree](posts/255.md)
- [285. Inorder Successor in BST](posts/285.md)
- [297. Serialize and Deserialize Binary Tree](posts/297.md)
- [298. Binary Tree Longest Consecutive Sequence](posts/298.md)
- [314. Binary Tree Vertical Order Traversal](posts/314.md)
- [331. Verify Preorder Serialization of a Binary Tree](posts/331.md)
- [426. Convert Binary Search Tree to Sorted Doubly Linked List](posts/426.md)
- [428. Serialize and Deserialize N-ary Tree](posts/428.md)
- [450. Delete Node in a BST](posts/450.md)
- [545. Boundary of Binary Tree](posts/545.md)
- [863. All Nodes Distance K in Binary Tree](posts/863.md)
- [987. Vertical Order Traversal of a Binary Tree](posts/987.md)

### 哈希表 (Hash Table)

- [30. Substring with Concatenation of All Words](posts/30.md)
- [49. Group Anagrams](posts/49.md)
- [146. LRU Cache](posts/146.md)
- [325. Maximum Size Subarray Sum Equals k](posts/325.md)
- [380. Insert Delete GetRandom O(1)](posts/380.md)
- [407. Trapping Rain Water II](posts/407.md)
- [438. Find All Anagrams in a String](posts/438.md)

### 深度优先搜索 (DFS)

- [17. Letter Combinations of a Phone Number](posts/17.md)
- [22. Generate Parentheses](posts/22.md)
- [37. Sudoku Solver](posts/37.md)
- [39. Combination Sum](posts/39.md)
- [40. Combination Sum II](posts/40.md)
- [51. N-Queens](posts/51.md)
- [52. N-Queens II](posts/52.md)
- [77. Combinations](posts/77.md)
- [78. Subsets](posts/78.md)
- [79. Word Search](posts/79.md)
- [90. Subsets II](posts/90.md)
- [131. Palindrome Partitioning](posts/131.md)
- [140. Word Break II](posts/140.md)
- [212. Word Search II](posts/212.md)
- [216. Combination Sum III](posts/216.md)
- [241. Different Ways to Add Parentheses](posts/241.md)
- [254. Factor Combinations](posts/254.md)
- [329. Longest Increasing Path in a Matrix](posts/329.md)
- [332. Reconstruct Itinerary](posts/332.md)
- [490. The Maze](posts/490.md)
- [505. The Maze II](posts/505.md)

### 宽度优先搜索 (Broad First Search, BFS)

- [126. Word Ladder II](posts/126.md)
- [127. Word Ladder](posts/127.md)
- [130. Surrounded Regions](posts/130.md)
- [133. Clone Graph](posts/133.md)
- [139. Word Break](posts/139.md)
- [200. Number of Islands](posts/200.md)
- [261. Graph Valid Tree](posts/261.md)
- [286. Walls and Gates](posts/286.md)
- [417. Pacific Atlantic Water Flow](posts/417.md)
- [505. The Maze II](posts/505.md)
- [743. Network Delay Time](posts/743.md)
- [1048. Longest String Chain](posts/1048.md)

### 拓朴排序 (Typographical Sort)

- [207. Course Schedule](posts/207.md)
- [210. Course Schedule II](posts/210.md)
- [310. Minimum Height Trees](posts/310.md)

### 二分查找 (Binary Search)

- [4. Median of Two Sorted Arrays](posts/4.md)
- [33. Search in Rotated Sorted Array](posts/33.md)
- [34. Find First and Last Position of Element in Sorted Array](posts/34.md)
- [50. Pow(x, n)](posts/50.md)
- [74. Search a 2D Matrix](posts/74.md)
- [81. Search in Rotated Sorted Array II](posts/81.md)
- [153. Find Minimum in Rotated Sorted Array](posts/153.md)
- [154. Find Minimum in Rotated Sorted Array II](posts/154.md)
- [162. Find Peak Element](posts/162.md)
- [274. H-Index](posts/274.md)
- [275. H-Index II](posts/275.md)
- [378. Kth Smallest Element in a Sorted Matrix](posts/378.md)
- [436. Find Right Interval](posts/436.md)
- [540. Single Element in a Sorted Array](posts/540.md)
- [668. Kth Smallest Number in Multiplication Table](posts/668.md)

### 栈 (Stack)

#### 括号匹配问题 (Parentheses Matching)

- [32. Longest Valid Parentheses](posts/32.md)
- [150. Evaluate Reverse Polish Notation](posts/150.md)
- [224. Basic Calculator](posts/224.md)
- [227. Basic Calculator II](posts/227.md)
- [394. Decode String](posts/394.md)

#### 单调栈问题 (Monotonic Stack)

- [42. Trapping Rain Water](posts/42.md)
- [84. Largest Rectangle in Histogram](posts/84.md)
- [85. Maximal Rectangle](posts/85.md)
- [239. Sliding Window Maximum](posts/239.md)
- [456. 132 Pattern](posts/456.md)

### 优先级队列/二叉堆 (Priority Queue / Heap)

- [135. Candy](posts/135.md)
- [347. Top K Frequent Elements](posts/347.md)
- [373. Find K Pairs with Smallest Sums](posts/373.md)
- [378. Kth Smallest Element in a Sorted Matrix](posts/378.md)
- [480. Sliding Window Median](posts/480.md)
- [692. Top K Frequent Words](posts/692.md)
- [973. K Closest Points to Origin](posts/973.md)

### 并查集 (Union Find)

- [200. Number of Islands](posts/200.md)
- [261. Graph Valid Tree](posts/261.md)
- [323. Number of Connected Components in an Undirected Graph](posts/323.md)
- [547. Friend Circles](posts/547.md)
- [695. Max Area of Island](posts/695.md)
- [947. Most Stones Removed with Same Row or Column](posts/947.md)

### 动态规划 (Dynamic Programming)

#### 记忆化搜索 (Memorization)

- [10. Regular Expression Matching](posts/10.md)
- [44. Wildcard Matching](posts/44.md)
- [87. Scramble String](posts/87.md)
- [494. Target Sum](posts/494.md)

#### 单序列型问题

- [5. Longest Palindromic Substring](posts/5.md)
- [72. Edit Distance](posts/72.md)
- [91. Decode Ways](posts/91.md)
- [132. Palindrome Partitioning II](posts/132.md)
- [152. Maximum Product Subarray](posts/152.md)
- [213. House Robber II](posts/213.md)
- [300. Longest Increasing Subsequence](posts/300.md) (耐心排序/Patience Sorting)
- [354. Russian Doll Envelopes](posts/354.md) (耐心排序/Patience Sorting)

#### 双序列型问题

- [97. Interleaving String](posts/97.md)
- [115. Distinct Subsequences](posts/115.md)

#### 矩阵路径问题

- [62. Unique Paths](posts/62.md)
- [63. Unique Paths II](posts/63.md)
- [64. Minimum Path Sum](posts/64.md)
- [174. Dungeon Game](posts/174.md)
- [221. Maximal Square](posts/221.md)
- [516. Longest Palindromic Subsequence](posts/516.md)

#### 背包问题 (Knapsack Problems)

- [322. Coin Change](posts/322.md)
- [410. Split Array Largest Sum](posts/410.md)
- [416. Partition Equal Subset Sum](posts/416.md)
- [474. Ones and Zeroes](posts/474.md)
- [518. Coin Change 2](posts/518.md)

#### 博弈问题

- [294. Flip Game II](posts/294.md)
- [486. Predict the Winner](posts/486.md)

#### 消去型问题

- [96. Unique Binary Search Trees](posts/96.md)
- [312. Burst Balloons](posts/312.md)
- [375. Guess Number Higher or Lower II](posts/375.md)

#### 其他问题

- [279. Perfect Squares](posts/279.md)

### 贪婪法 (Greedy)

- [45. Jump Game II](posts/45.md)
- [55. Jump Game](posts/55.md)
- [121. Best Time to Buy and Sell Stock](posts/121.md)
- [134. Gas Station](posts/134.md)
- [621. Task Scheduler](posts/621.md)

### 扫描线算法 (Swapping Lines)

- [56. Merge Intervals](posts/56.md)
- [57. Insert Interval](posts/57.md)
- [218. The Skyline Problem](posts/218.md)
- [253. Meeting Rooms II](posts/253.md)
- [986. Interval List Intersections](posts/986.md)

### 排序 (Sort)

#### 彩虹排序 (Rainbow Sort)

- [74. Search a 2D Matrix](posts/74.md)

#### 快速选择 (Quick Select)

- [86. Partition List](posts/86.md)
- [215. Kth Largest Element in an Array](posts/215.md)

#### 桶排序 (Bucket Sort)

- [164. maximum Gap](posts/164.md)

#### 其他问题

- [406. Queue Reconstruction by Height](posts/406.md)

### 前缀树/字典树 (Trie)

- [208. Implement Trie (Prefix Tree)](posts/208.md)
- [211. Add and Search Word - Data structure design](posts/211.md)
- [421. Maximum XOR of Two Numbers in an Array](posts/421.md)

### 线段树 (Segmentation Tree)

- [307. Range Sum Query - Mutable](posts/307.md)

### 树状数组 (Binary Indexed Tree, BIT)

- [307. Range Sum Query - Mutable](posts/307.md)
- [308. Range Sum Query 2D - Mutable](posts/308.md)
- [315. Count of Smaller Numbers After Self](posts/315.md)
