# 535. Encode and Decode TinyURL

```python
from base64 import b64encode, b64decode

class Codec:

    def encode(self, longUrl):
        """Encodes a URL to a shortened URL.

        :type longUrl: str
        :rtype: str
        """
        prefix, baseurl = longUrl.split('://')
        return prefix + '://' + b64encode(baseurl)

    def decode(self, shortUrl):
        """Decodes a shortened URL to its original URL.

        :type shortUrl: str
        :rtype: str
        """
        prefix, baseurl = shortUrl.split('://')
        return prefix + '://' + b64decode(baseurl)

# Your Codec object will be instantiated and called as such:
# codec = Codec()
# codec.decode(codec.encode(url))
```
