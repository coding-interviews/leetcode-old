# 103. Binary Tree Zigzag Level Order Traversal

与Binary Tree Level Order Traversal相比，只需要改动两行代码。

```cpp
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution
{
public:
    vector<vector<int>> zigzagLevelOrder(TreeNode* root)
    {
        if(!root)
            return {};
        vector<vector<int>> result;
        queue<TreeNode *> q({root});
        for(bool odd = true; !q.empty(); odd = !odd)
        {
            vector<int> level;
            level.reserve(q.size());
            for(int i = q.size() - 1; i >= 0; i--)
            {
                TreeNode *p = q.front();
                q.pop();
                level.push_back(p->val);
                if(p->left)
                    q.push(p->left);
                if(p->right)
                    q.push(p->right);
            }
            if(!odd)
                reverse(level.begin(), level.end());
            result.emplace_back(move(level));
        }
        return result;
    }
};
```
