# 120. Triangle

```cpp
class Solution
{
public:
    int minimumTotal(vector<vector<int>>& triangle)
    {
        if(triangle.empty())
            return 0;
        int N = triangle.size();
        for(int i = 1; i < N; i++)
        {
            triangle[i].front() += triangle[i - 1].front();
            for(int j = 1; j < i; j++)
                triangle[i][j] += min(triangle[i - 1][j - 1], triangle[i - 1][j]);
            triangle[i].back() += triangle[i - 1].back();
        }
        return *min_element(triangle.back().begin(), triangle.back().end());
    }
};
```
