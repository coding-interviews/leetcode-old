# 312. Burst Balloons

消去型动态规划。对于这类问题，我们可以倒着思考。为了方便，我们在前后各插入一个1。假设最后一个被扎破的气球是$`a_3`$，那么此时$`a_1,a_2,a_4,a_5`$都不存在了，所以得分是$`1\times a_3 \times 1`$。我们继续向上思考，此时整个数组被$`a_3`$分割成了左右两部分。由于$`a_3`$是最后被扎爆的，所以这时的$`a_3`$就像一堵墙，把数组分割成了左右两部分，这两部分各自的得分都互不影响。这样，$`a_3`$就把问题转换成了$`1\sim a_3`$和$`a_3\sim 1`$两个子问题。

```mermaid
graph TD;
c1((1))
a1((a1))
a2((a2))
a3((a3))
a4((a4))
a5((a5))
c2((1))
style c1 fill:lime
style c2 fill:lime
style a3 fill:lime
```

设$`f(i, j)`$表示扎爆$`i+1\sim j-1`$气球后的最大收益，那么递推公式为：

```math
f(i, j) = \max_{i<k<j}\left[f(i, k) + a_i a_k a_j + f(k, j)\right]
```

初始状态为$`f(i, i + 1) = 0`$，要求的目标值为$`f(0, N+1)`$。根据依赖关系，$`f(i, j)`$依赖于位于其左侧的$`f(i, k)`$和位于其下侧的$`f(k, j)`$，所以我们从左下角开始计算。

```cpp
class Solution
{
public:
    int maxCoins(vector<int>& nums)
    {
        if (nums.empty())
            return 0;
        nums.insert(nums.begin(), 1);
        nums.push_back(1);
        vector<vector<int>> dp(nums.size(), vector<int>(nums.size(), 0));
        for (int i = nums.size() - 1; i >= 0; i--)
            for (int j = i + 2; j < nums.size(); j++)
                for (int k = i + 1; k <= j - 1; k++)
                    dp[i][j] = max(dp[i][j], dp[i][k] + dp[k][j] + nums[i] * nums[k] * nums[j]);
        return dp.front().back();
    }
};
```
