# 251. Flatten 2D Vector

```cpp
class Vector2D
{
private:
    vector<vector<int>>::iterator it_row, end_row;
    vector<int>::iterator it;

public:
    Vector2D(vector<vector<int>>& v)
    {
        it_row = v.begin(), end_row = v.end();
        while(it_row != end_row && it_row->empty())
            ++it_row;
        if(it_row != end_row)
            it = it_row->begin();
    }

    int next()
    {
        int val = *it++;
        if(it == it_row->end())
        {
            do
                ++it_row;
            while(it_row != end_row && it_row->empty());
            if(it_row != end_row)
                it = it_row->begin();
        }
        return val;
    }

    bool hasNext()
    {
        return it_row != end_row;
    }
};

/**
 * Your Vector2D object will be instantiated and called as such:
 * Vector2D* obj = new Vector2D(v);
 * int param_1 = obj->next();
 * bool param_2 = obj->hasNext();
 */
```
