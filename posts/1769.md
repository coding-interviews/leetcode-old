# 1769. Minimum Number of Operations to Move All Balls to Each Box

## 暴力解法

题目限制$`1\le n \le 2000`$，所以$`O\left(N^2\right)`$的解法可以通过。

```cpp
class Solution
{
public:
    vector<int> minOperations(const string &boxes)
    {
        vector<int> operations(boxes.length(), 0);
        for(int i = 0; i < boxes.length(); i++)
            for(int j = 0; j < boxes.length(); j++)
            {
                if(i == j)
                    continue;
                operations[i] += (boxes[j] - '0') * abs(j - i);
            }
        return operations;
    }
};
```

## 优化解法

观察一下当$`i`$增加的时候，多了哪些操作，又少了那些操作。假设上一步操作后，$`i`$停留在了下面的位置，此时操作数为10。

```
0 1 0 1 1 0 1 1
      ↑
```

当指针向右移动的时候到下面位置的时候，箭头左边每个数字(0, 1, 0, 1)需要的操作数都需要加1，相当于加上了1倍的区间和($`0+1+0+1=2`$)；箭头及箭头右边的每个数字(1, 0, 1, 1)需要的操作数则需要减1，相当于减去了1倍的区间和($`1+0+1+1=3`$)。这样，新的操作数就变成了$`10+2-3=9`$。

```
0 1 0 1 1 0 1 1
        ↑
```

所以，我们只需要维护两个区间和`left`和`right`，当指针移动的时候，加上`left`并减去`right`，然后更新`left`和`right`即可。

```cpp
class Solution
{
public:
    vector<int> minOperations(const string &boxes)
    {
        vector<int> operations(boxes.length(), 0);
        for(int i = 1; i < boxes.length(); i++)
            operations[0] += (boxes[i] - '0') * i;
        
        int left = boxes[0] - '0', right = 0;
        for(int i = 1; i < boxes.length(); i++)
            right += boxes[i] - '0';

        for(int i = 1; i < boxes.length(); i++)
        {
            operations[i] = operations[i - 1] + left - right;
            left += boxes[i] - '0', right -= boxes[i] - '0';
        }
        return operations;
    }
};
```
