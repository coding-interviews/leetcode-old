# 465. Optimal Account Balancing

首先，n个人通过n次交易是一定可以保证每个人全部结清的：只需要从左到右两个人依次交易，每次交易保证左边的人余额清零即可。

其次，一个人和多个人交易不能得到更优的解。假设存在a, b, c三个人，三人的余额均不为0，那么交易次数将至少为2次：

- a和b交易：`a -= a, b += a;`
- b和c交易：`b -= b, c += b;`

如果a同时和另外两个人交易，那么a的余额将被分成两份，分别与b、c交易，然后b和c直接可能还需再交易零次或一次。无论如何，这种策略都不能得到比每个人只交易一次的最优解更好。

基于上述假设，我们将通过以下策略进行交易：

> 从左到右两个人依次交易，每次交易保证左边的人余额清零。

但是，这些人的排列顺序会影响交易次数，因为一旦某个人的余额变成了0，他就不需要再和右边的人交易了，这样交易次数就减少了一次。我们的任务就是找出这种最优的排列。实际上，我们没有很好的方法可以直接找出这种排列，只能一个一个去试。所以，对于某个人`start`，我们需要分别尝试跟右边的第`i`个人进行交易，使`start`的余额清零。通过类似深度优先搜索的方法，找出所有路径中最短的那一条。

```cpp
class Solution
{
private:
    int minTransfers(vector<int> &balance, int current, int transfers)
    {
        while(current < balance.size() && balance[current] == 0)
            current++;
        if(current == balance.size())
            return transfers;
        int minimal_transfers = INT_MAX;
        for(int next = current + 1; next < balance.size(); next++)
        {
            if(balance[current] > 0 && balance[next] < 0 ||
               balance[current] < 0 && balance[next] > 0)
            {
                balance[next] += balance[current];
                minimal_transfers = min(minimal_transfers,
                                        minTransfers(balance, current + 1, transfers + 1));
                balance[next] -= balance[current];
            }
        }
        return minimal_transfers;
    }

public:
    int minTransfers(vector<vector<int>>& transactions)
    {
        // 计算每个人的余额
        vector<int> balance(transactions.size(), 0);
        for(const vector<int> &transaction: transactions)
        {
            balance.resize(max(balance.size(), (size_t)(transaction[0] + 1)));
            balance.resize(max(balance.size(), (size_t)(transaction[1] + 1)));
            balance[transaction[0]] -= transaction[2];
            balance[transaction[1]] += transaction[2];
        }

        return minTransfers(balance, 0, 0);
    }
};
```
