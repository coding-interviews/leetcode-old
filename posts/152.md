# 152. Maximum Product Subarray

设$`f_{\max}(i)`$表示以$`x_i`$结尾的最大连续乘积，$`f_{\min}(i)`$表示以$`x_i`$结尾的最小连续乘积。则：

```math
f_{\max}(i) = \begin{cases}
f_{\max}(i - 1) \times x_i & x_i > 0 \\
0 & x_i = 0 \\
f_{\min}(i - 1) \times x_i & x_i < 0
\end{cases}
```

```math
f_{\min}(i) = \begin{cases}
f_{\min}(i - 1) \times x_i & x_i > 0 \\
0 & x_i = 0 \\
f_{\max}(i - 1) \times x_i & x_i < 0
\end{cases}
```

最终结果为：

```math
\max_i f_{\max}(i)
```

```cpp
class Solution
{
public:
    int maxProduct(vector<int> &nums)
    {
        // 设maxprod[i]表示以nums[i]结尾的最大连续乘积
        // 设minprod[i]表示以nums[i]结尾的最小连续乘积
        vector<int> maxprod(nums.size()), minprod(nums.size());
        maxprod[0] = minprod[0] = nums[0];
        for(int i = 1; i < nums.size(); i++)
        {
            maxprod[i] = minprod[i] = nums[i];
            if(nums[i] > 0)
            {
                maxprod[i] = max(maxprod[i], maxprod[i - 1] * nums[i]);
                minprod[i] = min(minprod[i], minprod[i - 1] * nums[i]);
            }
            else if(nums[i] < 0)
            {
                maxprod[i] = max(maxprod[i], minprod[i - 1] * nums[i]);
                minprod[i] = min(minprod[i], maxprod[i - 1] * nums[i]);
            }
        }
        return *max_element(maxprod.begin(), maxprod.end());
    }
};
```
