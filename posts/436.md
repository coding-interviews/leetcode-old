# 436. Find Right Interval

- 把开始时间从小到大排序
- 对于每个`interval`，在所有开始时间内二分查找大于等于`interval.end`的位置
- 该位置即为right interval，注意我们还需要找回在排序前数组中的坐标；如果找不到，则表明right interval不存在；

```cpp
class Solution
{
public:
    vector<int> findRightInterval(vector<vector<int>> &intervals)
    {
        map<int, int> startIdx;
        for(int i = 0; i < intervals.size(); i++)
            startIdx[intervals[i][0]] = i;

        vector<int> result;
        for (const vector<int> &interval: intervals)
        {
            auto it = startIdx.lower_bound(interval[1]);
            if(it != startIdx.end())
                result.push_back(it->second);
            else
                result.push_back(-1);
        }
        return result;
    }
};
```
