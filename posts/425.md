# 425. Word Squares

Word Square的定义是，满足对于任意$`i`$，第$`i`$行和第$`i`$列都相等的方阵。最直接的方法就是利用回溯法一行一行的去填。对于每一行，尝试所有没有用过的单词，验证一下是否符合要求，如果符合要求，就继续递归。

如果单词的数量为$`N`$，每个单词的长度为$`M`$，那么一共有$`N!`$中排列，对于每个排列我们需要验证每一行和每一列是否符合要求，所以总的时间复杂度应该是$`O(M\cdot N!)`$。

按照题目的数据量，$`N!=1000\approx 4.02\times 10^{2567}`$。这是一个天文数字，不可能完成，所以我们必须进行剪枝。

## 字典树

### 冗余一： 单词前缀必须与对应的列相同

试想一下，对于一个矩阵，如果我们已经填充了前两行，比如：

```
b a l l
a r e a
```

那么我们在填充第三行的时候，根据word square的要求，这一行的前两列至少要与第三列的前两行相同：

```
b a l l
a r e a
l e
```

所以，我们并不需要尝试所有的单词，而只需要尝试以`le`开头的单词。很显然，我们可以用一个字典树来找出所有以`le`开头的单词。

### 冗余二： 单词后缀所在列必须在字典中存在

除了前缀必须与对应的列相同，后缀其实也有额外的要求。还是回到刚才的例子，

```
b a l l
a
```

我们在填充第二行的时候，知道前缀必须跟第二列相同，但是也不能随便填。如果我们填了`area`，还必须保证字典里有前缀为`le`和第`la`的单词，否则填充第三行和第四行的时候就会无词可填。

```
 b a ⌈l⌉ ⌈l⌉
 a r ⌊e⌋ ⌊a⌋
[l e]
[l a]
```

经过这两项冗余的排除以后，计算量就降到了可以接受的程度。

```cpp
class Solution
{
private:
    class Trie
    {
    private:
        struct TrieNode
        {
            bool is_word = false;
            TrieNode* next[26] = { nullptr };
        } root;

        void addWord(const string& word)
        {
            TrieNode* p = &root;
            for (char ch : word)
            {
                if (p->next[ch - 'a'] == nullptr)
                    p->next[ch - 'a'] = new TrieNode;
                p = p->next[ch - 'a'];
            }
            p->is_word = true;
        }

        void listWords(const TrieNode* p, string& path, vector<string>& result) const
        {
            if (p->is_word)
            {
                result.push_back(path);
                return;
            }
            for (char ch = 'a'; ch <= 'z'; ch++)
                if (p->next[ch - 'a'])
                {
                    path.push_back(ch);
                    listWords(p->next[ch - 'a'], path, result);
                    path.pop_back();
                }
        }

    public:
        Trie(const vector<string>& words)
        {
            for (const string& word : words)
                addWord(word);
        }

        // 列出所有以prefix开头的单词
        vector<string> listWords(string& prefix) const
        {
            const TrieNode* p = &root;
            for (char ch : prefix)
            {
                if (p->next[ch - 'a'] == nullptr)
                    return {};
                p = p->next[ch - 'a'];
            }
            vector<string> result;
            listWords(p, prefix, result);
            return result;
        }

        // 字典中是否包含以prefix为前缀的单词
        bool contains(const string& prefix) const
        {
            const TrieNode* p = &root;
            for (char ch : prefix)
            {
                if (p->next[ch - 'a'] == nullptr)
                    return false;
                p = p->next[ch - 'a'];
            }
            return true;
        }
    };

    // 验证第row行的后缀是否符合要求
    bool validate(const Trie& trie, vector<string>& path, int row)
    {
        const int N = path.front().length();
        for (int j = row + 1; j < N; j++)
        {
            string s;
            for (int i = 0; i < row; i++)
                s.push_back(path[i][j]);
            if (!trie.contains(s))
                return false;
        }
        return true;
    }

    void dfs(const vector<string>& words, const Trie& trie, int row,
        vector<string>& path, vector<vector<string>>& result)
    {
        const int N = words.front().length();
        if (row >= N)
        {
            result.push_back(path);
            return;
        }
        // 尝试填第row行
        // 第row行的前row+1个字母应当与第row列的前row+1个字母相同
        string prefix;
        for (int i = 0; i < row; i++)
            prefix.push_back(path[i][row]);
        for (const string& word : trie.listWords(prefix))
        {
            path.push_back(word);
            if (validate(trie, path, row))
                dfs(words, trie, row + 1, path, result);
            path.pop_back();
        }
    }

public:
    /**
     * @param words: a set of words without duplicates
     * @return: all word squares
     */
    vector<vector<string>> wordSquares(vector<string>& words)
    {
        if (words.empty())
            return {};
        Trie trie(words);
        vector<string> path;
        vector<vector<string>> result;
        dfs(words, trie, 0, path, result);
        return result;
    }
};
```
