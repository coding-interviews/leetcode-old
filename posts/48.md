# 48. Rotate Image

```cpp
class Solution
{
public:
    void rotate(vector<vector<int>>& matrix)
    {
        if(matrix.empty())
            return;
        int N = matrix.size();
        // 先沿主对角线翻折（转置）
        for(int i = 0; i < N; i++)
            for(int j = i + 1; j < N; j++)
                swap(matrix[i][j], matrix[j][i]);
        // 翻转每一行
        for(vector<int> &row: matrix)
            reverse(row.begin(), row.end());
    }
};
```
