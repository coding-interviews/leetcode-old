# 843. Guess the Word

## Random Guess

最直接的方法就是瞎猜，如果相似度为6，那就猜对了；如果小于6，就从剩下的字符串中接着猜。题目说数组的长度最长为100，那么用脚指头也能想到，在10次以内猜中的概率肯定非常低。

我们就需要注意到一个事实，如果某单词`guess`和`secret`的相似度为$`n`$，那么列表中和`guess`的相似度不为$`n`$的单词一定可以排除。这可以通过逆否命题去思考：

> 设A和B的相似度为$`n`$，如果B和C相同，那么A和C的相似度也为$`n`$。

逆否命题：

> 设A和B的相似度为$`n`$，如果A和C的相似度不为$`n`$，那么B和C一定不相同。

其中，上述命题中，A就是猜测的单词`guess`，B是真正的`secret`，C是`wordlist`中的`secret`。

这样，我们就可以：

- 每次从列表中随机挑选一个单词`guess`，调用API计算与`secret`的相似度`similarity`。
- 从剩余单词中找出和`guess`相似度不等于`similarity`的单词，他们都可以被排除，从而更新单词列表。
- 重复上述过程，直到真正的`secret`被猜到。

如果列表中的单词是均匀分布的，那么每次迭代可以至少排除$`\frac{1}{6}`$的单词，通常不超过6次就能猜到`secret`。运气好的话，有可能一开始就猜到了相似度更高的单词，甚至能以更短的时间猜到答案。

但实际上，LeetCode给的test case并没有那么随机，如果每次只猜列表中第一个单词的话，有一些test case是无法通过的。因此，我们仍然需要`random_shuffle`，并每次从中随机选取单词。

```cpp
/**
 * // This is the Master's API interface.
 * // You should not implement it, or speculate about its implementation
 * class Master {
 *   public:
 *     int guess(string word);
 * };
 */
class Solution
{
public:
    void findSecretWord(vector<string>& wordlist, Master& master)
    {
        random_shuffle(wordlist.begin(), wordlist.end());
        for (int i = 0; i < 10; i++)
        {
            const string &guess = wordlist[rand() % wordlist.size()];
            int similarity = master.guess(guess);
            if (similarity == 6)
                break;
            vector<string> candidate;
            for (string& word : wordlist)
                if (numMatches(guess, word) == similarity)
                    candidate.push_back(move(word));
            wordlist = move(candidate);
        }
    }

    int numMatches(const string& s1, const string& s2)
    {
        int count = 0;
        for (int i = 0; i < s1.length(); i++)
            if (s1[i] == s2[i])
                count++;
        return count;
    }
};
```
