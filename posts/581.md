# 581. Shortest Unsorted Continuous Subarray

## 排序

最简单的方法就是对原数组排序，然后依次比较，就可以找到需要排序位置的起点和终点了。

```cpp
class Solution
{
public:
    int findUnsortedSubarray(vector<int>& nums)
    {
        vector<int> clone = nums;
        sort(clone.begin(), clone.end());
        int left = 0, right = nums.size() - 1;
        while(left < nums.size() && nums[left] == clone[left])
            left++;
        if(left == nums.size())
            return 0;
        while(left < right && nums[right] == clone[right])
            right--;
        return right - left + 1;
    }
};
```

## 单调栈

试想一下，给定一个数组`[1,3,5,2,7]`，如果我们从左到右扫描的话，一直到5都是已排好序的，而2明显比前面的数字小，所以它需要和前面的某些数字一起排序，那么排序的起点是哪里呢？我们需要向前找，找到第一个比2大的位置，很明显，这可以用一个单调递增栈来实现。

那么排序的终点怎么找呢？我们可以从右到左扫描数组，用一个单调递减栈来解决。那么，我们能否在从左到右扫描的过程中，顺便就把它找出来呢？换句话说，最后一个用来踢掉栈里元素的那个数字是不是排序的右边界呢？显然不是。考虑数组`[1,4,7,2,3]`，数字2踢掉了4和7，后续的3不再踢任何元素了，所以2是最后一个用来踢掉栈里元素的数字，但它显然不是右边界。

```cpp
class Solution
{
public:
    int findUnsortedSubarray(vector<int>& nums)
    {
        stack<int> s;
        int left = nums.size() - 1;
        for(int i = 0; i < nums.size(); i++)
        {
            while(!s.empty() && nums[s.top()] > nums[i])
            {
                left = min(left, s.top());
                s.pop();
            }
            s.push(i);
        }
        s = decltype(s)();
        int right = 0;
        for(int i = nums.size() - 1; i >= 0; i--)
        {
            while(!s.empty() && nums[s.top()] < nums[i])
            {
                right = max(right, s.top());
                s.pop();
            }
            s.push(i);
        }
        return left < right? right - left + 1: 0;
    }
};
```
