# 456. 132 Pattern

从右向左维护一个单调递减栈，同时记录一个次大值$`a_k`$。当从右向左遍历时，遇到一个新的更大的值时，把栈内的元素踢出去，同时更新次大值$`a_k`$（最后一次踢出的元素）；如果遇到较小的值，则比较是否小于次大值$`a_k`$，若小于次大值，则该元素即为$`a_i`$，否则放入单调栈中。

```cpp
class Solution
{
public:
    bool find132pattern(vector<int>& nums)
    {
        stack<int> s;
        for(int i = nums.size() - 1, ak = INT_MIN; i >= 0; i--)
            if(nums[i] < ak)
                return true;
            else
            {
                while(!s.empty() && nums[i] > s.top())
                {
                    ak = s.top();
                    s.pop();
                }
                s.push(nums[i]);
            }
        return false;
    }
};
```
