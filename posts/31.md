# 31. Next Permutation

求取下一个排列通常需要以下几步：

1. 从右向左，找到第一个变小的位置，记为`left`。若不存在，则表明当前已经是最后一个排列。
2. 从右向左，找到第一个比`nums[left]`大的位置，记为`right`。
3. 交换`nums[left]`和`nums[right]`。
4. 翻转`nums[left]`后面的所有数字。

例如，对于序列`nums = [1,2,4,5,5,5,4,3,3]`，

1. 从右向左找到第一个变小的位置。

    ```
    1 2 [4] 5 5 5 4 3 3
         ↑
        left
    ```

2. 从右向左找到第一个大于`nums[left]`的位置。

    ```
    1 2 [4] 5 5 [5] 4 3 3
         ↑       ↑
        left   right
    ```

3. 交换`nums[left]`和`nums[right]`。

    ```
    1 2 [5] 5 5 [4] 4 3 3
         ↑       ↑
        left   right
    ```

4. 翻转`nums[left]`后面的所有数字

    ```
    1 2 [5] [3 3 4 4 5 5]
         ↑
        left
    ```

```cpp
class Solution
{
public:
    void nextPermutation(vector<int>& nums)
    {
        int left = nums.size() - 2;
        while(left >= 0 && nums[left] >= nums[left + 1])
            left--;
        if(left >= 0)
        {
            int right = nums.size() - 1;
            while(nums[left] >= nums[right])
                right--;
            swap(nums[left], nums[right]);
        }
        reverse(nums.begin() + left + 1, nums.end());
    }
};
```

**参考：** C++标准库`next_permutation`函数的实现 <https://zh.cppreference.com/w/cpp/algorithm/next_permutation>
