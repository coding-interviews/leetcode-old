# 1. Two Sum

**时间复杂度：** $`O(N)`$

**空间复杂度：** $`O(N)`$

```cpp
class Solution
{
public:
    vector<int> twoSum(vector<int>& nums, int target)
    {
        unordered_map<int, int> dict;
        for(int i = 0; i < nums.size(); i++)
        {
            if(dict.count(target - nums[i]))
                return {dict[target - nums[i]], i};
            dict[nums[i]] = i;
        }
        return {-1, -1};
    }
};
```
