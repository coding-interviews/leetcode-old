# 1135. Connecting Cities With Minimum Cost

最小生成树的Kruskal算法

1. 设顶点的数量为$`n`$。
2. 创建一个大小为$`n`$的不相交集。
3. 把初始边的集合按权重从小到大排序。
4. 从小到大遍历边的集合，直到遍历完整个集合或者生成树的边数达到$`n-1`$。如果两个顶点不属于同一个集合：
    - 合并这两个集合
    - 在最终的生成树中添加这一条边，并记下边的数量。
5. 如果生成树边的数量小于$`n-1`$，则表面该无向图不是连通图，不存在生成树。

```cpp
class Solution
{
private:
    int find(vector<int> &uf, int x)
    {
        if(uf[x] < 0)
            return x;
        else
            return uf[x] = find(uf, uf[x]);
    }

public:
    int minimumCost(int n, vector<vector<int>>& connections)
    {
        vector<int> uf(n + 1, -1);
        sort(connections.begin(), connections.end(), [](const vector<int> &v1, const vector<int> &v2)
        {
            return v1[2] < v2[2];  // 按权值从小到大排序
        });

        int cost = 0, n_edges = 0;
        for(int i = 0; i < connections.size() && n_edges < n - 1; i++)
        {
            int c1 = find(uf, connections[i][0]), c2 = find(uf, connections[i][1]);
            if(c1 != c2)
            {
                uf[c1] = c2;
                cost += connections[i][2];
                n_edges++;
            }
        }
        return n_edges == n - 1? cost: -1;
    }
};
```

Kruskal算法每次要从都要从剩余的边中选取一个最小的边。通常我们要先对边按权值从小到大排序，这一步的时间复杂度为为$`O(|E|\log|E|)`$。Kruskal算法的实现通常使用并查集，来快速判断两个顶点是否属于同一个集合。最坏的情况可能要枚举完所有的边，此时要循环$`|E|`$次，所以这一步的时间复杂度为$`O(|E|\alpha(V))`$，其中$`\alpha`$为Ackermann函数，其增长非常慢，我们可以视为常数。所以Kruskal算法的时间复杂度为$`O(|E|\log|E|)`$。
