# 56. Merge Intervals

```cpp
class Solution
{
public:
    vector<vector<int>> merge(vector<vector<int>>& intervals)
    {
        struct Line
        {
            int time;
            enum TYPE { START, END } type;
            Line(int time, TYPE type): time(time), type(type) {}
        };
        vector<Line> lines;
        for(vector<int> &interval: intervals)
        {
            lines.push_back({interval[0], Line::START});
            lines.push_back({interval[1], Line::END});
        }
        sort(lines.begin(), lines.end(), [](const Line &l1, const Line &l2)
        {
            // time相同时，START排在前面，这样可以避免将[1,4],[4,5]拆分成两段
            return l1.time < l2.time || l1.time == l2.time && l1.type < l2.type;
        });

        vector<vector<int>> result;
        int overlaps = 0;   // 当前时间段重叠的数量
        for(Line &line: lines)
        {
            if(line.type == Line::START)
            {
                if(overlaps == 0)    // overlaps由0变为1
                    result.push_back({line.time, 0});
                overlaps++;
            }
            else
            {
                overlaps--;
                if(overlaps == 0)    // overlaps由1变为0
                    result.back()[1] = line.time;
            }
        }
        return result;
    }
};
```
