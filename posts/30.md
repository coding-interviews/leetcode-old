# 30. Substring with Concatenation of All Words

## Hash表

我们注意到，每个单词的长度是相同的。对于区间$`[i, i + km)`$，正好装上$`k`$个字符串，所以我们可以把数组`words`转成Hash表，然后分别查找区间$`[i, i + m), [i + m, i + 2m),\ldots, [i + km)`$的单词是否在Hash表中（注意单词出现的次数也要相同）。

在Hash表中查找一个单词的时间复杂度是$`O(m)`$，一共需要查找$`k`$个单词，起始位置一共有$`n`$个，所以总的时间复杂度是$`O(nmk)`$。

```cpp
class Solution
{
public:
    vector<int> findSubstring(const string &s, vector<string>& words)
    {
        if (s.empty() || words.empty())
            return {};
        unordered_map<string, int> counter;
        for (const string &word : words)
            counter[word]++;
        vector<int> result;
        for (int i = 0, m = words.front().length(), k = words.size(); i + k * m <= s.length(); i++)
        {
            unordered_map<string, int> window;
            for (int j = 0; j < k; j++)
            {
                string word = s.substr(i + j * m, m);
                if (counter.count(word))
                    window[word]++;
            }
            if (window == counter)
                result.push_back(i);
        }
        return result;
    }
};
```

## 滑动窗法

区间$`[0, km)`$和$`[m, (k+1)m)`$的区别只是左边少了一个单词，右边多了一个单词，所以我们只需要稍微修改一下`window`中这两个单词的计数值（时间复杂度仅为$`O(2m)`$），而无需扫描整个长度为$`km`$的区间（时间复杂度$`O(km)`$）。

这样，对于起始位置$`i, i + m, \ldots, i + (\frac{n}{m} - 1)m`$，实际只有$`i=0`$时需要执行时间复杂度为$`O(km)`$的完全扫描操作，其余$`\frac{n}{m} - 1`$次操作每次的时间复杂度只有$`O(m)`$，加起来一共是$`O(km + n)`$。由于$`i`$的取值范围一共有$`m`$个值，所以总的时间复杂度是$`O\left(km^2 + mn\right)`$。相比于原解法$`O(kmn)`$，由于$`m \ll n`$，时间复杂度大幅降低。

```cpp
class Solution
{
public:
    vector<int> findSubstring(const string &s, vector<string>& words)
    {
        if (s.empty() || words.empty())
            return {};
        unordered_map<string, int> counter;
        for (const string &word : words)
            counter[word]++;
        vector<int> result;
        for (int i = 0, n = s.length(), m = words.front().length(), k = words.size(); i < m; i++)
        {
            unordered_map<string, int> window;
            // 对区间[i, i + k * m)执行一次完全扫描
            for (int j = 0; j < k; j++)
            {
                string word = s.substr(i + j * m, m);
                if (counter.count(word))
                    window[word]++;
            }
            if (window == counter)
                result.push_back(i);
            // 对区间[i + t * m, i + (t + 1) * m)进行快速操作
            for (int t = 1; i + (t + k) * m <= n; t++)
            {
                string left = s.substr(i + (t - 1) * m, m), right = s.substr(i + (t + k - 1) * m, m);
                if (counter.count(left))
                    window[left]--;
                if (counter.count(right))
                    window[right]++;
                if (window == counter)
                    result.push_back(i + t * m);
            }
        }
        return result;
    }
};
```
