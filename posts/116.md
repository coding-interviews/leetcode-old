# 116. Populating Next Right Pointers in Each Node

## 层次遍历

最直接的方法是利用二叉树的层次遍历，但这样需要一个大小约为一层节点数量的队列。

```cpp
/*
// Definition for a Node.
class Node {
public:
    int val;
    Node* left;
    Node* right;
    Node* next;

    Node() {}

    Node(int _val, Node* _left, Node* _right, Node* _next) {
        val = _val;
        left = _left;
        right = _right;
        next = _next;
    }
};
*/
class Solution
{
public:
    Node* connect(Node* root)
    {
        if(!root)
            return nullptr;
        queue<Node *> q({root});
        while(!q.empty())
        {
            Node dummyhead(0), *p = &dummyhead;
            for(int i = q.size() - 1; i >= 0; i--)
            {
                p->next = q.front();
                p = p->next;
                q.pop();
                if(p->left)
                    q.push(p->left);
                if(p->right)
                    q.push(p->right);
            }
        }
        return root;
    }
};
```

## 不使用额外空间

我们同时维护两个指针：`parent`和`current`。`parent`所在的一层已经把链表串好了，我们在遍历`parent`链表的时候，把脚下的一层串起来，并挂在`dummyhead`后面。遍历完这一层以后，`dummyhead.next`就是下一层的开始，把`parent`指针指向那里，就可以遍历下一层了。

开始的时候，`parent`指向`root`，`current`指向`dummyhead`。`root`所在层只有一个节点，也可以看作已经串好了。我们一层一层地遍历，直到下一层的链表为空。

![leetcode](https://assets.leetcode.com/uploads/2019/02/15/117_sample.png)

```cpp
/*
// Definition for a Node.
class Node {
public:
    int val;
    Node* left;
    Node* right;
    Node* next;

    Node() {}

    Node(int _val, Node* _left, Node* _right, Node* _next) {
        val = _val;
        left = _left;
        right = _right;
        next = _next;
    }
};
*/
class Solution
{
public:
    Node* connect(Node* root)
    {
        Node dummyhead(0);
        for(Node *parent = root, *current = &dummyhead; parent; )
        {
            if(parent->left)
                current = current->next = parent->left;
            if(parent->right)
                current = current->next = parent->right;
            if(parent->next)
                parent = parent->next;
            else
            {
                parent = dummyhead.next;
                current = &dummyhead;
                current->next = nullptr;
            }
        }
        return root;
    }
};
```
